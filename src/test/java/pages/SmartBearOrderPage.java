package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;
import java.util.List;

public class SmartBearOrderPage {

    public SmartBearOrderPage(WebDriver driver){
        PageFactory.initElements(driver, this);
    }

    @FindBy(id = "ctl00_MainContent_fmwOrder_ddlProduct")
    public WebElement productDropDown;

    @FindBy(xpath = "//input[@id='ctl00_MainContent_fmwOrder_txtQuantity']")
    public WebElement quantityInputBox;

    @FindBy(id = "ctl00_MainContent_fmwOrder_txtName")
    public WebElement customerName;

    @FindBy(id = "ctl00_MainContent_fmwOrder_TextBox2")
    public WebElement streetName;

    @FindBy(id = "ctl00_MainContent_fmwOrder_TextBox3")
    public WebElement cityName;

    @FindBy(id = "ctl00_MainContent_fmwOrder_TextBox4")
    public WebElement stateName;

    @FindBy(id = "ctl00_MainContent_fmwOrder_TextBox5")
    public WebElement zip;

    @FindBy(id = "ctl00_MainContent_fmwOrder_cardList_0")
    public WebElement cardTypeVisa;

    @FindBy(id = "ctl00_MainContent_fmwOrder_TextBox6")
    public WebElement cardNumber;

    @FindBy(id = "ctl00_MainContent_fmwOrder_TextBox1")
    public WebElement expireDate;

    @FindBy(id = "ctl00_MainContent_fmwOrder_InsertButton")
    public WebElement processButton;

    @FindBy(id = "//input[@id='ctl00_MainContent_orderGrid_ctl02_OrderSelector']//parent::td//parent::tr//td")
    public List<WebElement> selectedRowInfo;

    @FindBy(tagName = "h2")
    public WebElement secondHeader;

    public void aa(List<String> checkInfo){
        checkInfo.add(customerName.getText());
        checkInfo.add(quantityInputBox.getText());
        checkInfo.add(streetName.getText());
        checkInfo.add(cityName.getText());
        checkInfo.add(stateName.getText());
        checkInfo.add(zip.getText());
        checkInfo.add(cardTypeVisa.getText());
        checkInfo.add(cardNumber.getText());
        checkInfo.add(expireDate.getText());
    }

    public void selectFromDropDown(String productName){
        Select select = new Select(productDropDown);
        select.selectByVisibleText(productName);
    }
}
