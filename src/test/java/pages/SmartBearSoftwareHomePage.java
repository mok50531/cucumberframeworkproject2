package pages;

import org.junit.Assert;
import org.openqa.selenium.NotFoundException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import java.util.List;

public class SmartBearSoftwareHomePage {

    public SmartBearSoftwareHomePage(WebDriver driver){
        PageFactory.initElements(driver, this);
    }

    @FindBy(css = "#ctl00_menu>li>a")
    public List<WebElement> viewAllElements;

    @FindBy(id = "ctl00_MainContent_btnCheckAll")
    public WebElement checkAllButton;

    @FindBy(id = "ctl00_MainContent_btnUncheckAll")
    public WebElement uncheckAllButton;

    @FindBy(xpath = "//input[@type='checkbox']")
    public List<WebElement> checkBoxes;

    @FindBy(id = "ctl00_MainContent_btnDelete")
    public WebElement deleteAllButton;

    @FindBy(id = "ctl00_MainContent_orderMessage")
    public WebElement listIsEmptyMessage;

    public void selectFromList(String linkText){
        for (WebElement viewAllElement : viewAllElements) {
            if (linkText.equals(viewAllElement.getText())) viewAllElement.click();
        }
    }
}
