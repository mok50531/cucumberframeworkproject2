package utilities;

import cucumber.api.Scenario;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;

public class ScreenShotUtil {

    public static void takeScreenShot(Scenario scenario, WebDriver driver){
        try {
            byte[] screenShot = ((TakesScreenshot) driver).getScreenshotAs(OutputType.BYTES);
            scenario.embed(screenShot, "img/png");
        }catch (Exception e){
            e.printStackTrace();
        }
    }
}
