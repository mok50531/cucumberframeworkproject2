package steps;

import com.github.javafaker.Faker;
import cucumber.api.java.Before;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import io.cucumber.datatable.DataTable;
import org.junit.Assert;
import org.openqa.selenium.NotFoundException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import pages.SmartBearOrderPage;
import pages.SmartBearSoftwareHomePage;
import pages.SmartBearSoftwareLoginPage;
import utilities.Driver;
import utilities.Waiter;

import java.util.List;

public class SmartBearPageValidate {

    WebDriver driver;
    SmartBearSoftwareLoginPage smartBearSoftwareLoginPage;
    SmartBearSoftwareHomePage smartBearSoftwareHomePage;
    SmartBearOrderPage smartBearOrderPage;

    @Before
    public void setUp(){
        driver = Driver.getDriver();
        smartBearSoftwareLoginPage = new SmartBearSoftwareLoginPage(driver);
        smartBearSoftwareHomePage = new SmartBearSoftwareHomePage(driver);
        smartBearOrderPage = new SmartBearOrderPage(driver);
    }

    @Given("user is on {string}")
    public void userIsOn(String url) {
        driver.get(url);
    }

    @When("user enters username as {string}")
    public void userEntersUsernameAs(String username) {
        smartBearSoftwareLoginPage.usernameInputBox.sendKeys(username);
    }

    @And("user enters password as {string}")
    public void userEntersPasswordAs(String password) {
        smartBearSoftwareLoginPage.passwordInputBox.sendKeys(password);
    }

    @And("user clicks on Login button")
    public void userClicksOnLoginButton() {
        smartBearSoftwareLoginPage.loginButton.click();
    }

    @Then("user should see {string} Message")
    public void userShouldSeeMessage(String errorMessage) {
        Assert.assertTrue(smartBearSoftwareLoginPage.errorMessage.isDisplayed());
        Assert.assertEquals(errorMessage, smartBearSoftwareLoginPage.errorMessage.getText());
    }

    @Then("user should be routed to {string}")
    public void userShouldBeRoutedTo(String url) {
        Assert.assertEquals(url, driver.getCurrentUrl());
    }

    @And("validate below menu items are displayed")
    public void validateBelowMenuItemsAreDisplayed(DataTable dataTable) {
        List<String> expectedResults = dataTable.asList();
        for (int i = 0; i < expectedResults.size(); i++) {
            Assert.assertTrue(smartBearSoftwareHomePage.viewAllElements.get(i).isDisplayed());
            Assert.assertEquals(expectedResults.get(i), smartBearSoftwareHomePage.viewAllElements.get(i).getText());
        }
    }

    @When("user clicks on {string} button")
    public void userClicksOnButton(String button) {
        WebElement element;
        switch (button){
            case "Check All":
                element = smartBearSoftwareHomePage.checkAllButton;
                break;
            case "Uncheck All":
                element = smartBearSoftwareHomePage.uncheckAllButton;
                break;
            case "Process":
                element = smartBearOrderPage.processButton;
                break;
            case "Delete Selected":
                element = smartBearSoftwareHomePage.deleteAllButton;
                break;
            default:
                throw new NotFoundException(button + " is not found");
        }
        element.click();
    }

    @Then("all rows should be checked")
    public void allRowsShouldBeChecked() {
        for (int i = 0; i < smartBearSoftwareHomePage.checkBoxes.size(); i++) {
            Assert.assertTrue(smartBearSoftwareHomePage.checkBoxes.get(i).isSelected());
        }
    }

    @Then("all rows should be unchecked")
    public void allRowsShouldBeUnchecked() {
        for (int i = 0; i < smartBearSoftwareHomePage.checkBoxes.size(); i++) {
            Assert.assertFalse(smartBearSoftwareHomePage.checkBoxes.get(i).isSelected());
        }
    }

    @When("user clicks on {string} menu item")
    public void userClicksOnMenuItem(String menuButton) {
        smartBearSoftwareHomePage.selectFromList(menuButton);
    }

    @And("user selects {string} as product")
    public void userSelectsAsProduct(String product) {
        smartBearOrderPage.selectFromDropDown(product);
    }

    @And("user enters {int} as quantity")
    public void userEntersAsQuantity(int quantity) {
        smartBearOrderPage.quantityInputBox.sendKeys(String.valueOf(quantity));
    }

    @And("user enters all address information")
    public void userEntersAllAddressInformation() {
        smartBearOrderPage.customerName.sendKeys("Alex");
        smartBearOrderPage.streetName.sendKeys("Des Plains");
        smartBearOrderPage.cityName.sendKeys("Chicago");
        smartBearOrderPage.stateName.sendKeys("Illinois");
        smartBearOrderPage.zip.sendKeys("60852");
    }

    @And("user enters all payment information")
    public void userEntersAllPaymentInformation() {
        smartBearOrderPage.cardTypeVisa.click();
        smartBearOrderPage.cardNumber.sendKeys("1234567890123456");
        smartBearOrderPage.expireDate.sendKeys("01/25");
    }

    @Then("user should see their order displayed in the {string} table")
    public void userShouldSeeTheirOrderDisplayedInTheTable(String header) {
        Assert.assertEquals(header, smartBearOrderPage.secondHeader.getText());
    }

    @And("validate all information entered displayed correct with the order")
    public void validateAllInformationEnteredDisplayedCorrectWithTheOrder() {

    }

    @Then("validate all orders are deleted from the {string}")
    public void validateAllOrdersAreDeletedFromThe(String arg0) {
        
    }

    @And("validate user sees {string} Message")
    public void validateUserSeesMessage(String message) {
        Assert.assertTrue(smartBearSoftwareHomePage.listIsEmptyMessage.isDisplayed());
        Assert.assertEquals(message, smartBearSoftwareHomePage.listIsEmptyMessage.getText());
    }
}
